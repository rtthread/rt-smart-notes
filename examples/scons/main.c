#include <stdio.h>
#include <rtthread.h>

static void entry(void* parameter)
{
    rt_kprintf("hello world!\n");
}

int main(int argc, char** argv)
{
    rt_thread_t tid;

    tid = rt_thread_create("hello", 
        entry, NULL, 
        4096, 20, 10);
    if (tid != NULL)
    {
        rt_thread_startup(tid);
    }

    rt_thread_mdelay(100);

    return 0;
}
