#include <stdio.h>
#include <pthread.h>

void *pthread_entry(void* parameter)
{
    printf("hello world\n");

    return NULL;
}

int main(int argc, char** argv)
{
    int ret;
    void *value;
    pthread_t pth;

    /* 创建pthread线程来执行后续的hello输出 */
    ret = pthread_create(&pth, NULL, pthread_entry, NULL);
    printf("ret = %d\n", ret);

    /* 等待结束 */
    pthread_join(pth, &value);

    return 0;
}
