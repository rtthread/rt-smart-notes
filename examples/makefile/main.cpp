#include <iostream>
#include <vector>

#include <stdio.h>

extern "C" {

int main(int argc, char** argv)
{
    int index = 0;

    std::vector <int> a;
    for (index = 0; index < 5; index ++)
    {
        a.push_back(index);
    }

    for(std::vector<int>::iterator it=a.begin(); it!= a.end(); it++)
        printf("index=%d\n", *it);

    return 0;
}

}
