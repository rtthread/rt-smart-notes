# SDK版本

目前rt-smart的版本都是通过网络共享的方式放出，目前新版本的链接是：

[rt-smart sdk](http://117.143.63.254:9012/www/rt-smart/rt-smart-20210312.zip)

而如果需要新版本的工具链，可以通过以下链接来获得

* [Linux版本的ARM32位工具链](http://117.143.63.254:9012/www/rt-smart/arm-linux-musleabi_for_x86_64-pc-linux-gnu_stable.tar.bz2)
* [Windows版本的ARM32位工具链](http://117.143.63.254:9012/www/rt-smart/arm-linux-musleabi_for_i686-w64-mingw32_stable.zip)
* [Linux版本的AArch64位工具链](http://117.143.63.254:9012/www/rt-smart/aarch64-linux-musleabi_for_x86_64-pc-linux-gnu_stable.zip)
* [Windows版本的AArch64位工具链](http://117.143.63.254:9012/www/rt-smart/aarch64-linux-musleabi_for_i686-w64-mingw32_stable.zip)

目前新版本的工具链都会携带内核态、用户态的libc，不需要额外在rt-smart sdk中放置对应的libc。所以也可直接使用gitee上rt-smart分支最新版本。
