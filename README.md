# RT-Thread Smart技术笔记

一些文章列表

* [【ART-Pi Smart】上手体验以及 vscode 插件使用](https://club.rt-thread.org/ask/article/3267.html)
* [【ART-Pi Smart】使用 VS Code 开发 GUI 应用](https://club.rt-thread.org/ask/article/3304.html)
* [【ART-Pi Smart】基于 FFmpeg + SDL2 实现视频播放](https://club.rt-thread.org/ask/article/3332.html)
* [【ART-Pi Smart】基于 SDL2 进行游戏开发](https://club.rt-thread.org/ask/article/3380.html)
* [在“正点原子-阿尔法”开发板使用RT-Thread Smart](https://club.rt-thread.org/ask/article/3377.html)
* [ART-Pi Smart驱动的适配](https://club.rt-thread.org/ask/article/3368.html)


树莓派上的情况

* [树莓派上使用RT-Thread Smart](https://www.rt-thread.org/document/site/#/rt-thread-version/rt-thread-smart/rt-smart-quickstart/rt-smart-quickstart)